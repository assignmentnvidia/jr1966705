import os.path

import pandas as pd
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
pd.set_option('display.max_rows',100)
pd.set_option('display.max_columns', 300)
import time
from sklearn.model_selection import train_test_split
from ydata_profiling import ProfileReport
import seaborn as sns
import matplotlib.pyplot as plt
import sklearn
import argparse

def calc_roc(y_true, y_pred):
    #create manual ROC graph
    thresholds = np.logspace(-9, -5, 100)  # Adjust the number of thresholds as needed
    tpr_values = []
    fpr_values = []
    y_true = np.squeeze((y_true).astype(int))
    for threshold in thresholds:
        y_pred_thresholded = np.squeeze((y_pred >= threshold).astype(int))

        # Calculate True Positives (TP), False Positives (FP), True Negatives (TN), and False Negatives (FN)
        TP = np.sum((y_true == 1) & (y_pred_thresholded == 1))
        FP = np.sum((y_true == 0) & (y_pred_thresholded == 1))
        TN = np.sum((y_true == 0) & (y_pred_thresholded == 0))
        FN = np.sum((y_true == 1) & (y_pred_thresholded == 0))
        # Calculate TPR and FPR
        tpr = TP / (TP + FN)
        fpr = FP / (FP + TN)
        tpr_values.append(tpr)
        fpr_values.append(fpr)
    #create plot
    import matplotlib.pyplot as plt
    plt.figure(figsize=(8, 6))
    plt.plot(fpr_values, tpr_values, label='ROC Curve')
    plt.xlabel('False Positive Rate (FPR)')
    plt.ylabel('True Positive Rate (TPR)')
    plt.title('ROC Curve')
    plt.grid(True)
    plt.legend()
    # Save ROC figure to a file
    figpath = os.path.join((os.path.dirname(data_path)),'roc_fig.png')
    plt.savefig(figpath, dpi=300, bbox_inches='tight')
    print(' roc figure saved to '+figpath)
    return tpr_values, thresholds

def calc_metrics(y_true, y_pred,thr):
    from sklearn.metrics import confusion_matrix, precision_score, recall_score, precision_recall_fscore_support, \
        balanced_accuracy_score, accuracy_score, roc_curve

    # Calculate balanced accuracy score
    print('confusion matrix')
    cm = confusion_matrix(y_true, y_pred, normalize='true')
    for row in cm:
        formatted_row = ["{:.3f}".format(value) for value in row]
        print(" ".join(formatted_row))
    print('precision_score = ' + str(precision_score(y_true, y_pred)))
    print('recall_score = ' + str(recall_score(y_true, y_pred)))
    print('balanced_accuracy_score = ' + str(balanced_accuracy_score(y_true, y_pred)))
    print('accuracy_score = ' + str(accuracy_score(y_true, y_pred)))
    print('num of minority in test = ' + str(np.sum(y_true > thr)))

def data_manipulation(training,remove_corr=True, important_only=False):
    ## ydata profiling report to file, was used for offline EDA
    # from ydata_profiling import ProfileReport
    # profile = ProfileReport(training,title="Profiling Report")
    # source_file = "/raw_data_profile_full.html"
    # profile.to_file(source_file)

    training = training.replace('nan', np.nan)
    training = training.dropna(subset=['TLJYWBE'])

    ## Columns with all NaN values:
    null_columns = training.columns[training.isnull().all()]
    null_columns_list = null_columns.tolist()
    print("Columns with all null values:", str(len(null_columns_list)))

    training = training.drop(columns=null_columns_list)

    null_columns = training.columns[training.isna().all()]
    null_columns_list = null_columns.tolist()
    print("Columns with all NaN values:", str(len(null_columns_list)))
    training = training.drop(columns=null_columns_list)

    # Remove the constant columns from the DataFrame
    constant_cols = training.columns[training.nunique(dropna=False) == 1]
    print("Columns with all constant:", str(len(constant_cols)))
    # print(constant_cols)
    training = training.drop(columns=constant_cols)
    # Remove the constant columns from the DataFrame which also include nans

    ##remove cols with corr > 0.95 to others - #calculated beforehand and kept the values it takes a lot of time
    # float64_columns = training.select_dtypes(include=['float64'])
    # corr_matrix = float64_columns.corr().abs()
    ## Select upper triangle of correlation matrix
    # upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(bool))
    # corr_to_drop = [column for column in upper.columns if any(upper[column] > 0.95)]
    corr_to_drop = ['GPCKISJ', 'BFYTKKS', 'TDMCRGU', 'UIDFQDA', 'JORKKSF', 'VEUJYWN', 'SOLTBDE', 'KGJACPV', 'VPAZKWG',
                    'RRMMRAJ', 'DAEPCOR', 'HGHMTVH', 'MWHYOSB', 'HWKCDPO', 'LKFNGDB', 'QJXZQSD', 'QWVFRRZ', 'EYILHOU',
                    'JHQKHHL', 'MIIVLBT', 'QVOHUMP', 'VCSGLFK', 'LFVWFTF', 'RCJGZLT', 'CWWUCQG', 'YBBDEJH', 'KGPPPLD',
                    'GGTSHYD', 'LCXZSFV', 'REQAQFJ', 'EBCCQTL', 'POPBDDN', 'NYRKOCF', 'DNXCPJX', 'JKCFMAY', 'RTVQHPO',
                    'XNHNHIK', 'MRBOALK', 'BFVUEAT', 'WTFITDY', 'QVSCEFJ', 'MNWQYTP', 'TFWEIKV', 'NFZEWMH', 'NAWMDDT',
                    'FUTFIRO', 'LGVRVXB', 'SPKCFPP', 'ZCRFMLI', 'KEJOIIS', 'AEXRRBM', 'PVYZFWG', 'BZFTTYE', 'AGTCLZR',
                    'JGSHLSI', 'OULTOYT', 'WTMETCB', 'LNZGNNA', 'ILHBFRB', 'HVHDNTE', 'UZXVNSW', 'PKCUSYS', 'MKVXAUF',
                    'HOAJWUR', 'NJFNKOX', 'GJFZDWF', 'SRDJVGB', 'OAEOBHM', 'OELFGBD', 'NMCAKWK', 'DLWYUSK', 'VSXTBCN',
                    'XWIRHVW', 'IIYLALO', 'NGUTDOX', 'FVFVALZ', 'UCXNWDN', 'OENIIAU', 'JTIJWNL', 'XWQPLHB', 'CMACFHX',
                    'AHBBVON', 'HHSFOPV', 'TXVCNZC', 'JZQULTU', 'BGHDMAS', 'HPCWOKU', 'MQXCIBE', 'FBBFYZM', 'BHSHCHU',
                    'XLVHGLO', 'JIOWSAX', 'QRTDBHR', 'PCYIKPD', 'DNHHKRL', 'ZIWVRVM', 'PJWUWBJ', 'HHWCKEO', 'INJPVHJ',
                    'LVMHJCI', 'SLHDFLB', 'IFZDTPK', 'TBFLCFW', 'KUXSPYJ', 'KKFJKFD', 'JZFIGSE', 'BUVBEYO', 'KYRXYOZ',
                    'CMSGVTL', 'FXACWUA', 'LJWUXOV', 'YLHQTEA', 'UNFEABR', 'DMTXDGF', 'MRUWBZT', 'PFFFNMW', 'VEOALEL',
                    'NSERJIK', 'NYWUAUO', 'JNEVNAR', 'LGXZTHS', 'ZZTPIJA', 'VYTOISY', 'RMVEJRV', 'NVBZJEU', 'BTXMHGA',
                    'SMOGFZD', 'VCPMKTP', 'LLOQQSF', 'DPPXTGF', 'XQGPAIG', 'SDEUDHY', 'INLEXOC', 'VUTOMKG', 'XOGKHIO',
                    'HDNPBLR', 'UZQXSAH', 'LCJYFNQ', 'IWHIYNA', 'PHWLHGO', 'OPBTWFJ', 'KFDAVVI', 'UFUYDJX', 'QJKCJMC',
                    'SMHHBFH', 'OFEEPAC', 'TKNRHFX', 'YWXXBRX', 'LTRIIBH', 'ZXTCPRN', 'YCJYNVQ', 'VJNUBJX', 'MDBYIRV',
                    'UGXVMKE', 'KRFHHDX', 'ZUEEBTY', 'SHLZRCB', 'UILLRWJ', 'RTYTHAC', 'VSPXQWL', 'LZIQISI', 'DEAUOMN',
                    'NLRRCWZ', 'TZNKMJP', 'CHWVXPB', 'CFAIUJQ', 'OMPWYAA', 'QYDTWHL', 'CKBVBUJ', 'USEHIHH', 'IPRULOU',
                    'YKMYFOT', 'FCMPHJR', 'FAPNOHE', 'DGVMUBH', 'QXYDOGR', 'WTCICYP', 'WBBKQFH', 'WLFAYHX', 'DZZXXVG',
                    'JYSKSPX', 'BGEQHWR', 'XIVOAXI', 'CKTRRMV', 'MXNLYUW', 'NFRPIGQ', 'UBMPCGB', 'PTGZANY', 'KPZEEJR',
                    'OOVNYOI', 'TBGAWDK', 'GRBGIZR', 'NGIJPET', 'LQEHWDR', 'GRYCMJK', 'XIKNUBQ', 'EMIXPGQ', 'OXRCIYW',
                    'YVAAWCN', 'UTZNPAJ', 'UWOLTWR', 'LCWSCVA', 'RLYWHCU', 'DTSZUFG', 'MOGJHYG', 'HSCCVTR', 'VMHLOFU',
                    'BTDJSVV', 'KCVNCWE', 'BBVLSAK', 'NRZNVUW', 'KKMOSNN', 'KYJTYYF', 'QDZIZBN', 'YRLOEIR', 'UZIGIGC',
                    'DTJITAO', 'TPUASCQ', 'AHDJJHN', 'JWXNCNT', 'NQYIWSN', 'RHSKKTW', 'DDPBVDN', 'DITVBVV', 'QALTYYP',
                    'UZGUYFK', 'TKQUNLP', 'HCSXZKW', 'WHHQYVO', 'NTJRVGR', 'ETSCGYD', 'LLHXSPF', 'OOXHPWM', 'ZHFQXTN',
                    'QKXDWEZ', 'CBNASKU', 'EKTAHFF', 'EYARVDG', 'WTARSLQ', 'AWMGPPL', 'RXLZZHO', 'SJZKEVZ', 'UAEETIT',
                    'UDQKCAP', 'GYFZSXY', 'OGYQNUB', 'GVJTVXV', 'RIEGYBR', 'BGPVFMN', 'DXWRFDA', 'BZYFPMM', 'JQXEEPS',
                    'NVITMSQ', 'FTMZZVG', 'PZYGETW', 'WBCMBKW', 'YIDCRAN', 'XPCRCSI', 'LZBJHYC', 'ARVLGNZ', 'VUBFVUN',
                    'JKILSTV', 'RFVSOEI', 'OPLXQJS', 'VAGMJKH', 'SHXMZYS', 'LTDFQFZ', 'LTZYUVU', 'IUCJIOI', 'WVRSHBW',
                    'SFTKCKG', 'KXISVUL', 'HGXUJOI', 'ULYIQYO', 'BYEQVGG', 'STMKCSJ', 'SRQKQTX', 'FAIOOOV', 'DCDCWXQ',
                    'DIKRMOL', 'WPYBJPG', 'GUEUYTS', 'HBCMCGN', 'ODXOWHT', 'MOSECGI', 'PSXOLCG', 'UWFUPRN', 'GQHLWWM',
                    'SHBNDLU']
    print('columns with 0.95 corr = ' + str(len(corr_to_drop)))
    if remove_corr:
        training.drop(corr_to_drop, axis=1, inplace=True)

    # encode nan values with one value to try it as a category
    from sklearn.preprocessing import LabelEncoder
    label_encoder = LabelEncoder()
    # print('const column with nans, can be check later as additional class')
    constant_cols = training.columns[training.nunique() == 1]
    print("Columns with all constant+nan:", str(len(constant_cols)))
    for i in constant_cols:
        training[i] = label_encoder.fit_transform(training[i])
    # training = training.drop(columns=constant_cols)

    ## option to change low unique float to categories - not tested
    # low_uniq = training.columns[training.nunique() < 20]
    # training[low_uniq] = training[low_uniq].astype('category')

    # # Change object-type columns to categories\numerical
    object_columns = training.select_dtypes(include=['object'])
    for i in object_columns.columns:
        ## if hight unique values it is float that was changed to string
        if len(training[i].unique()) > 3:
            training[i] = training[i].astype(float)
        else:
            # if it's low unique values we asuume categories
            keep = training[i].isna()
            training[i] = label_encoder.fit_transform(training[i])
            training[i].iloc[keep] = np.nan
            training[i] = training[i].astype('category')

    # Change int-type columns to category
    int64_columns = training.select_dtypes(include=['int64'])
    training[int64_columns.columns] = training[int64_columns.columns].astype('category')

    ## option to use only the top features by  importance

    if important_only:
        target = ['TLJYWBE']
        keep_important = ['MSAHYEA', 'IKMWIOV', 'VZOZKWX', 'JMFGDPB', 'FFJOGRA', 'XMVIFQS', 'KPQSPBC', 'RZPQSGM',
                          'AIKOJYC', 'DJANPLY', 'PDWQQDP', 'LCFCVCB', 'LNWVWPK', 'SKKXRWR', 'AKUNFFN', 'AVSMOFQ',
                          'IMTAJAV', 'PYGQGOY', 'NUTXJCG', 'YCYSGLG', 'IHCEXCN', 'RFLFROK', 'KDQUJOB', 'UTGKBXG',
                          'DVSZBLN', 'LIXROQF']
        training = training[keep_important + target]

    return training

def main(important_only, val_and_test, train_choose, sample_to_minority, generate_pos, remove_corr, data_path,
         data_outputpath):
    # Your script logic here
    print("important_only:", important_only)
    print("val_and_test:", val_and_test)
    print("train_choose:", train_choose)
    print("sample_to_minority:", sample_to_minority)
    print("generate_pos:", generate_pos)
    print("remove_corr:", remove_corr)
    print("data_path:", data_path)
    print("data_outputpath:", data_outputpath)
    thr = 1e-5
    # important_only = False
    # val_and_test = False
    # train_choose = 'reg'
    # sample_to_minority = False
    # generate_pos = False
    # remove_corr = True
    # data_path='/homes/omriy/Downloads/tabular.feather'
    # data_outputpath ='/homes/omriy/Downloads/'


    #load data
    training = pd.read_feather(data_path)

    final_data = data_manipulation(training,remove_corr=remove_corr, important_only=important_only)
    if data_outputpath:
        import os
        import json
        final_data.to_csv(os.path.join(data_outputpath,'manipulated_data.csv'), index=False)
        type_dict = final_data.dtypes.apply(lambda x: x.name).to_dict()
        with open(os.path.join(data_outputpath,'manipulated_data.json'), 'w') as json_file:
            json.dump(type_dict, json_file)



    ## preparing the data to training
    final_data['y'] =(final_data['TLJYWBE']>=thr).astype(int)
    final_data = final_data.dropna(subset=['TLJYWBE'])
    final_data['rand'] = np.random.rand(len(final_data))

    final_data.reset_index(drop=True)
    X = final_data.drop(['y','TLJYWBE'], axis =1)
    y_reg = final_data[['TLJYWBE']]
    y_class = final_data[['y']]


    ## training step
    import lightgbm as lgb
    print(X.shape)

    from sklearn.model_selection import train_test_split
    if train_choose == 'reg':
        print('regression model')
        if val_and_test:
            #split to train\val\test, use threshold from val on test
            X_train, X_test, y_train, y_test = train_test_split(X, y_reg, test_size=0.1, random_state=12)
            X_train, y_train, y_val = train_test_split(X_train, y_train, test_size=0.09, random_state=12)
        else:
            X_train, X_test, y_train, y_test = train_test_split(X, y_reg, test_size=0.2, random_state=23)
            X_val = X_test
            y_val = y_test

        ## training with sample of 108 where all the failtest are in and simmilar of pass
        if sample_to_minority:
            pos = X_train[np.array(y_train>= thr)]
            neg = X_train.sample(n=len(pos), random_state=42)
            X_train = pd.concat([neg, pos])
            y_train = pd.concat([y_train.sample(n=len(pos), random_state=42), y_train[np.array(y_train>= thr)]])
            # Reset the index (optional)
            X_train.reset_index(drop=True, inplace=True)
            y_train.reset_index(drop=True, inplace=True)

        #use SMOGN (SMOTE for regression) to augment data for more testfail examples
        if generate_pos:
            import smogn
            X_train_add = X_train.copy()
            X_train_add['TLJYWBE'] = y_train
            X_train_up = X_train_add[X_train_add['TLJYWBE']>np.log(5e-6)]
            for i in range(5):
                training_smogn = smogn.smoter(data=X_train_up.reset_index(drop=True), y='TLJYWBE')
                pos_addons = training_smogn[training_smogn['TLJYWBE']>np.log(1e-5)]
                X_train_add = pd.concat([X_train_add,pos_addons], ignore_index=True)
            y_train = X_train_add[['TLJYWBE']]
            X_train = X_train_add.drop(['TLJYWBE'], axis =1)


        print('shape of train data')
        print(X_train.shape)
        print('shape of test data')
        print(X_test.shape)

        #lightGBM parameters for regression with tweedie objective
        params = {
            'boosting_type': 'gbdt',
            'random_state': 23,
            'objective': 'tweedie',
            'num_leaves': 1000,  # A large number of leaves
            'max_depth': -1,     # Infinite depth
            'min_data_in_leaf': 1,
            'metric': ['l1', 'l2'],
            'verbosity': 1
        }
        ## train & predict part
        train_data = lgb.Dataset(X_train, label=y_train)
        test_data = lgb.Dataset(X_test, label=y_test)
        num_round = 100

        start_time = time.time()
        bst = lgb.train(params, train_data, num_round, valid_sets=test_data)
        end_time = time.time()
        y_pred = bst.predict(X_val, num_iteration=bst.best_iteration)

        ## evaluation of results
        y_true = np.array(y_val)
        y_true = (y_true >= thr).astype(int)
        print('len of test  '+str(len(y_test)))
        y_train = np.array(y_train)

        #based on tpr wanted range, calculate confusion matrix and other metrics
        y_true = np.array(y_test)
        y_true = (y_true >= thr).astype(int)
        y_pred = bst.predict(X_test, num_iteration=bst.best_iteration)

        tpr_values, thresholds = calc_roc(y_true, y_pred)

        max_index = len(tpr_values) - np.argmax(tpr_values[::-1]) - 1
        y_pred_bin = (y_pred >= thresholds[max_index]).astype(int)
        print(thresholds[max_index])
        calc_metrics(y_true, y_pred_bin,thr)

        tpr_values = np.squeeze(tpr_values)
        condition = (tpr_values > 0.7) & (tpr_values < 0.9)
        thr80 = np.where(condition)[0][-1]
        y_pred_bin = (y_pred >= thresholds[thr80]).astype(int)
        print(thresholds[thr80])
        calc_metrics(y_true, y_pred_bin, thr)

        #calc run time
        training_time = end_time - start_time
        print(f"Training time: {training_time:.2f} seconds")

    elif train_choose == 'kfold':
        print('k fold cross validation classifier')
        from sklearn.model_selection import StratifiedKFold, cross_val_score
        n_splits = 5
        y = y_class
        if sample_to_minority:
            pos = X[np.array(y_class>= thr)]
            neg = X.sample(n=len(pos), random_state=42)
            X = pd.concat([neg, pos])
            y = pd.concat([y_class.sample(n=len(pos), random_state=42), y_class[np.array(y_class>= thr)]])
            # Reset the index (optional)
            X.reset_index(drop=True, inplace=True)
            y.reset_index(drop=True, inplace=True)

            print('shape of train data')
            print(X.shape)

            # Set hyperparameters for the LightGBM regressor
        lgb_model = lgb.LGBMClassifier(
            boosting_type='gbdt',
            objective='binary',
            num_leaves=1000,  # A large number of leaves
            max_depth=-1,  # Infinite depth
            min_data_in_leaf=1,
            metric='binary_logloss',
            scale_pos_weight=10000,
            verbosity=0,
            # categorical_feature='auto',
            random_state=42
        )

        cv = cross_val_score(lgb_model, X, y, scoring='balanced_accuracy', cv=5)

        print('balanced_accuracy Cross Val')
        print(cv)
        print(cv.mean())
        bst = lgb_model._best_iteration
        exit(0)
        ## if we want to use predict on val after train\test\val after split
        # cv = cross_val_score(lgb_model, X_train, y_train, scoring='balanced_accuracy', cv=5)
        # y_true = np.array(y_test)
        # y_pred = bst.predict(X_test, num_iteration=bst.best_iteration)
        #
        # from sklearn.metrics import roc_curve
        #
        # fpr, tpr, thresholds = roc_curve(y_true, y_pred)
        # y_pred = (y_pred > 0.5).astype(int)
        #
        # calc_metrics(y_true, y_pred, 0.5)


    elif train_choose == 'grid':
        print('grid search classifier')
        from sklearn.model_selection import train_test_split
        X_train, X_test, y_train, y_test = train_test_split(X, y_class, test_size=0.2, stratify=y_class, random_state=42)
        print('single classifier')
        lgb_model = lgb.LGBMClassifier(
            boosting_type = 'gbdt',
            objective = 'binary',
            num_leaves= 1000,  # A large number of leaves
            max_depth= -1,  # Infinite depth
            min_data_in_leaf= 1,
            metric= 'binary_logloss',
            scale_pos_weight= 10000,
            verbosity= 0,
            random_state=42
        )

        grid_params = {
            'boosting': ['gbdt' ],
            'objective': ['binary'],
            'num_iterations': [  100  ],
            # 'learning_rate':[  0.1, 0.01 ],
            'min_data_in_leaf': [ 1,  20],
            # 'num_leaves':[ 20,30, 40 ],
            'max_depth' :[ 5, 25, 1000],
            'scale_pos_weight': [1000, 5000, 10000],
            # 'min_child_samples': [10, 20, 30],
            # 'feature_fraction': [ 0.6, 0.8,  0.9, 0.7],
            # 'bagging_fraction': [  0.6, 0.8 ],
            # 'bagging_freq': [ 0, 100, ],
        }
        from sklearn.model_selection import GridSearchCV
        gsearch_lgb = GridSearchCV(lgb_model, param_grid = grid_params, cv=5, scoring='balanced_accuracy', verbose=1)
        y_train = np.squeeze(np.array(y_train))
        gsearch_lgb.fit(X_train, y_train)
        # lgb_model.cv_results_

        print('best params')
        # latest best
        # {'bagging_fraction': 0.6, 'bagging_freq': 100, 'boosting': 'gbdt', 'feature_fraction': 0.8, 'learning_rate': 0.1,
        #  'max_depth': 25, 'min_data_in_leaf': 25, 'num_iterations': 100, 'num_leaves': 30, 'objective': 'binary',
        #  'scale_pos_weight': 10000}

        print (gsearch_lgb.best_params_)
        y_true = np.array(y_test)
        y_pred = gsearch_lgb.predict(X_test)
        bst = gsearch_lgb


        # Convert probabilities to binary predictions (0 or 1)
        from sklearn.metrics import roc_curve
        fpr, tpr, thresholds = roc_curve(y_true, y_pred)
        y_pred = (y_pred > 0.5).astype(int)

        calc_metrics(y_true, y_pred, 0.5)
    elif train_choose=='classification':
        print('single classifier')

        X_train, X_test, y_train, y_test = train_test_split(X, y_class, test_size=0.2, stratify=y_class, random_state=12)

        ## training with sample of 108 where all the failtest are in and simmilar of pass
        if sample_to_minority:
            pos = X_train[np.array(y_train>0.5)]
            neg = X_train.sample(n=len(pos), random_state=42)
            X_train = pd.concat([neg, pos])
            y_train = pd.concat([y_train.sample(n=len(pos), random_state=42), y_train[np.array(y_train>0.5)]])
            # Reset the index (optional)
            X_train.reset_index(drop=True, inplace=True)
            y_train.reset_index(drop=True, inplace=True)

        print(X_train.shape)
        print(X_test.shape)

        lgb_model = lgb.LGBMClassifier(
            boosting_type='gbdt',
            objective='binary',
            num_leaves=1000,  # A large number of leaves
            max_depth=-1,  # Infinite depth
            min_data_in_leaf=1,
            metric='binary_logloss',
            scale_pos_weight=10000,
            verbosity=0,
            random_state=42
        )
        params = {
            'boosting_type': 'gbdt',
            'objective': 'binary',
            'num_leaves': 1000,  # A large number of leaves
            'max_depth': -1,  # Infinite depth
            'min_data_in_leaf': 1,
            'metric': ['binary_logloss'],
            # 'is_unbalance': 'true',
            'scale_pos_weight': 10000,
            'verbosity': 0
        }

        train_data = lgb.Dataset(X_train, label=y_train)
        num_round = 100
        bst = lgb.train(params, train_data, 100, categorical_feature='auto')

        y_true = np.array(y_test)
        y_pred = bst.predict(X_test, num_iteration=bst.best_iteration)

        # visualization using yellowbrick package
        print('visualize using yellowbrick package')
        from yellowbrick.classifier import ROCAUC
        visualizer = ROCAUC(lgb_model,binary=True)

        visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
        visualizer.score(X_test, y_test)  # Evaluate the model on the test data
        visualizer.show()

        ##Convert probabilities to binary predictions (0 or 1)
        from sklearn.metrics import roc_curve
        fpr, tpr, thresholds = roc_curve(y_true, y_pred)
        y_pred_binary = (y_pred > 0.5).astype(int)
        # plt.plot()
        calc_metrics(y_true, y_pred_binary,0.5)


    ## feature importance
    feature_importances = bst.feature_importance(importance_type='split')  # 'split' or 'gain'
    feature_ranking = pd.DataFrame({'Feature': X.columns.values.tolist(), 'Importance': feature_importances})
    # Sorting
    feature_ranking = feature_ranking.sort_values(by='Importance', ascending=False)
    feature_ranking.reset_index(drop=True, inplace=True)
    # we added a random feature and we want to measure how many feature add more value
    rows_with_rand = feature_ranking[feature_ranking['Feature'] == 'rand']
    rand_importance = int(rows_with_rand['Importance'].iloc[0])
    only_important = feature_ranking[feature_ranking['Importance'] > rand_importance]
    print('number of important than random features:')
    print(len(list((only_important['Feature']))))
    # print(list((only_important['Feature'])))
    if data_outputpath:
        import os
        important_data = final_data[only_important['Feature']]
        important_data.to_csv(os.path.join(data_outputpath,'important_data.csv'), index=False)
        type_dict = important_data.dtypes.apply(lambda x: x.name).to_dict()
        with open(os.path.join(data_outputpath,'important_data.json'), 'w') as json_file:
            json.dump(type_dict, json_file)



def parse_args():
    # Argument parser setup
    parser = argparse.ArgumentParser(description='Script description.')

    # Add arguments
    parser.add_argument('--important_only', action='store_true', default=False, help='use saved important_only features')
    parser.add_argument('--val_and_test', action='store_true', default=False, help='do regression with val_and_test ')
    parser.add_argument('--train_choose', type=str, default='reg', choices=['reg', 'kfold', 'grid', 'classification'], help='Description of train_choose argument')
    parser.add_argument('--sample_to_minority', action='store_true', default=False, help='sample the major part of data to minority size')
    parser.add_argument('--generate_pos', action='store_true', default=False, help='generate more minority examples using SMOGN ')
    parser.add_argument('--remove_corr', action='store_true', default=True, help='remove data that is very correlated for dataset')
    parser.add_argument('--data_path', type=str, default='/homes/omriy/Downloads/tabular.feather', help='input file data path')
    parser.add_argument('--data_outputpath', type=str, default='', help='path to save dataset after manipulations, none for not saving')

    # Parse command-line arguments
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = parse_args()
    main(args.important_only, args.val_and_test, args.train_choose, args.sample_to_minority, args.generate_pos,
         args.remove_corr, args.data_path, args.data_outputpath)